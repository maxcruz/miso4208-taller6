Feature: Register into losestudiantes
    As an new user I want to register myself within losestudiantes website in order to rate teachers

Scenario Outline: Register incomplete

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill incomplete the register form with <correo1>, <password2>, <nombre3>, <apellido4> and <acepta5>
    And I try to register
    Then I expect to see an error

  Examples:
      | correo1          | password2 | nombre3 | apellido4 | acepta5 | 
      |                  | Fooo      | Fooo    | Fooo      | Si      |
      | fooo@foo.com     |           | Fooo    | Fooo      | Si      |
      | fooo@foo.com     | Fooo      |         | Fooo      | Si      |
      | fooo@foo.com     | Fooo      | Fooo    |           | Si      |
      | fooo@foo.com     | Fooo      | Fooo    | Fooo      | No      |
      |                  |           |         |           | No      |

Scenario Outline: Register already exist

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill the register form with an existing <user>
    And I try to register
    Then I expect to be notified that the user <exist>

  Examples:
    | user                    | exist                                                                          |
    | mr.cruz@uniandes.edu.co | Error: Ya existe un usuario registrado con el correo 'mr.cruz@uniandes.edu.co' |

Scenario: Register successful

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill the register form for a new user
    And I try to register
    Then I expect to be registered

