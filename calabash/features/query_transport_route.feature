Feature: Query transport routes

  Scenario: As a user I want to open the route main screen
    # Close onboarding
    Given I press "Paraderos"
    And I wait for 2 seconds
    And I press "Rutas de buses"
    Then I should see "TRANSMILENIO"
    Then I should see "URBANO"
    Then I should see "COMPLEMENTARIO"

  Scenario: As a user I want to search a complementary bus
    Given I wait for 2 seconds
    And I press "Rutas de buses"
    Then I should see "COMPLEMENTARIO"
    And I press "COMPLEMENTARIO"
    And I press "Search"
    And I enter "18-3" into input field number 1
    And I press the enter button
    Then I should see "Auto Norte - Est. Terminal"
    Then I should see "Germania"

  Scenario: As a user I want to see the route detail for a specific bus
    Given I wait for 2 seconds
    And I press "Rutas de buses"
    And I press "Search"
    And I enter "J72" into input field number 1
    And I press the enter button
    Then I should see "Portal Norte"
    Then I should see "Universidades"
    And I touch the "Portal Norte" text
    Then I should see "Recorrido:"
    Then I should see "L-V"
    Then I should see "S"
    Then I should see "Portal Norte"
    And I press "Portal Norte"
    Then I should see "Detalle: Auto Norte"
    

