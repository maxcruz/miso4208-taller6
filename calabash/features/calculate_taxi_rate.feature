Feature: Calculate taxi rate

  Scenario: As a user I want to open the calculator of taxi rate
    # Close onboarding
    Given I press "Paraderos"               
    When I swipe left
    And I wait for 2 seconds
    And I press "Calcular tarifa taxi"
    Then I should see "Unidades"

  Scenario: As a user I want to calculate the taxi fee for 100 units
    Given I wait for 2 seconds
    When I swipe left
    And I wait for 2 seconds
    And I press "Calcular tarifa taxi"
    And I enter "100" into input field number 1
    Then I should see "$8,900"

  Scenario: As a user I want to calculate the taxi fee for 100 units at night
    Given I wait for 2 seconds
    When I swipe left
    And I wait for 2 seconds
    And I press "Calcular tarifa taxi"
    And I press "Noct./Fest."
    And I enter "100" into input field number 1
    Then I should see "$10,900"

  Scenario: As a user I want to calculate the taxi fee for 100 units at night to the airport
    Given I wait for 2 seconds
    When I swipe left
    And I wait for 2 seconds
    And I press "Calcular tarifa taxi"
    And I press "Noct./Fest."
    And I press "Aeropuerto"
    And I enter "100" into input field number 1
    Then I should see "$15,000"

  Scenario: As a user I want to see the taxi rate details
    Given I wait for 2 seconds
    When I swipe left
    And I wait for 2 seconds
    And I press "Calcular tarifa taxi"
    And I press "Detalle de recargos"
    Then I should see "Recargos"
    Then I should see "$4100"
    Then I should see "$82"


